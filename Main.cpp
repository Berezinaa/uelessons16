#include <iostream>
#include <time.h>

int main() {
	const int size = 5;
	int array[size][size];

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			array[i][j] = i + j;
		}
	}

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			std::cout << array[i][j] << " ";
		}
		std::cout << "\n";
	}

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int lineIndex = buf.tm_mday % size;
	std::cout << "Elements sum for line #" << lineIndex << ": ";

	int summ = 0;
	for (int j = 0; j < size; j++)
	{
		summ = summ + array[lineIndex][j];
	}
	std::cout << summ;
}